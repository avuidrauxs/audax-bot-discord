const Discord = require('discord.js');
const logger = require('winston');
const dotenv = require('dotenv');

dotenv.config();

logger.remove(logger.transports.Console);
logger.add(logger.transports.Console, {
    colorize: true
});
logger.level = 'debug';


// Initialize Discord Bot
const bot = new Discord.Client();

bot.on('message', message => {
    var prefix = '!'
    var msg = message.content;

    if (msg === prefix + 'Vegeta') {
        message.channel.send('Message that goes above image', {
            files: [
                "https://vignette.wikia.nocookie.net/dragonball/images/a/a1/Vegeta-manga.jpg/revision/latest/scale-to-width-down/220?cb=20170822020417"
            ]
        });
    }
});

bot.login(process.env.AUDAX_BOT_TOKEN);

bot.on('ready', function (evt) {
    logger.info('Connected');
    logger.info('Logged in as: ');
    logger.info(bot.user.username + ' - (' + bot.user.id + ')');
});
